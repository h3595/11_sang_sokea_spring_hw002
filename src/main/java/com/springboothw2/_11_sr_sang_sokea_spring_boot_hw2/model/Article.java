package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Data
public class Article {
    private int id;
    @NotEmpty(message = "Please provide a title.")
    private String title;
    @NotEmpty(message = "Please provide a description for your article.")
    private String description;
    private String urlImg;
    private MultipartFile file;

    public Article(int id, String title, String description, String urlImg) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.urlImg = urlImg;
    }
}
