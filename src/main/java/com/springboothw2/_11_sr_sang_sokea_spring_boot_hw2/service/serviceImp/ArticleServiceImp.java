package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.serviceImp;
import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.model.Article;
import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.repository.ArticleRepository;
import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAllArticle();
    }

    @Override
    public Article findArticleById(int id) {
        return articleRepository.findArticleById(id);
    }

    @Override
    public void addArticleMethod(Article article) {
        articleRepository.addNewArticle(article);
    }

    @Override
    public void updateArticleMethod(int id, Article article) {
        System.out.println("in service: " + id + " " + article);
        articleRepository.updateArticleById(id, article);
    }

    @Override
    public void deleteArticleMethod(int id) {
        articleRepository.deleteArticleById(id);
    }
}
