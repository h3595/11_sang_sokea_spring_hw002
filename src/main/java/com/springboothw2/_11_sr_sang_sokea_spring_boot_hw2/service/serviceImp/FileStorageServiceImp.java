package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.serviceImp;

import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService {

    Path fileStorageLocation;

    FileStorageServiceImp(){
        fileStorageLocation = Paths.get("src/main/resources/images");
    }

    @Override
    public String saveFile(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();

        if(filename.contains("..")){
            System.out.println("Wrong format of the file.");
            return null;
        }

        String[] fileParts = filename.split("\\.");

        filename = UUID.randomUUID() + "." + fileParts[1];

        Path resolvePath = fileStorageLocation.resolve(filename);
        Files.copy(file.getInputStream(), resolvePath, StandardCopyOption.REPLACE_EXISTING);
        return filename;
    }
}
