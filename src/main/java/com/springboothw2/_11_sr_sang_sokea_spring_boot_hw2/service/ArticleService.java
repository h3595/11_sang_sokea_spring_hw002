package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service;

import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.model.Article;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    public List<Article> getAllArticle();
    public Article findArticleById(int id);
    public void addArticleMethod(Article article);
    public void updateArticleMethod(int id, Article article);
    public void deleteArticleMethod(int id);
}