package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorageService {
    String saveFile(MultipartFile file) throws IOException;
}
