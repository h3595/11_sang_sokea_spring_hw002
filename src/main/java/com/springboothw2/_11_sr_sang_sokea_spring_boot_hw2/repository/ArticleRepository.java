package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.repository;

import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepository {
    List<Article> articles = new ArrayList<>();
    public ArticleRepository(){
        Article article1 = new Article(1,"Spring", "this spring description", "https://res.cloudinary.com/practicaldev/image/fetch/s--zrUJwvgZ--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/bupbqc9fctvw4j7r14it.png");
        Article article2 = new Article(2,"React Native", "this React Native description", "https://www.futuremind.com/m/cache/c8/15/c8150d863e584ed42ccfbdc3f3f1aa3a.jpg");
        Article article3 = new Article(3,"JavaScript", "this JavaScript description", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAABblBMVEXy4hslMjjQwC347pUGBwkAAAgAAADv55a+vL/OwD8jLzXr7O7T1NXe0BkEAwYICwzOvSH475r45xw/UlwzQ0vZy08TFxobIyb/9pnXxi7z5UXh1mzt5Y/16WXTxTny4yPIxsebmpoAGzAdLDaVlGfj4MUNIjLr6MMfLzPdzSfEvnwEHzHl3Y0AFy9pbVUAEy6uq3JwY38AACpugV4sOTx3i2McKTVFVEhXZ1FpXnfd1ol/gFw0Qj8AKTlXWFmlpaWNjY1qbnBeb1ZTPkRsRkyYVFpMcYF+b0tPTF5DYm9UfY41PEd/bY1leps9R0xJWEmfnmtudnp1bRAuLQs/OguLghJChWNFnGk9S1pkXVDwkBgtMDI9cVtQUEm3dCStWWC3mVqhiFNrYkcmOkeXrXSOom+kWF9JO0KASlJ0aUlHWG1UaIEMJydejaBqn7aQfFDAn12HW09XR0KlaVicgKh1eFjs6LDp6dzAu6HCvaAPsksdAAAMcklEQVR4nO2djX/axh3GfSZusmubpEadrxtrAkdVVZg3SzoJUDHGmACyEU7WLlnnNXZD7dpNvXT11v33+51kEvyGeXNOtvXwsRCSILlvnnvuRYczMxMqVKhQoUKFChUqVKhQoUKFChUqVKhQoUKFChUqVKhQoUKFChUqVKhQoUKFChUqVEA0/+hjoXo0L5rACHo6J1hPRRMYXs/m5u4I1dzcX0UzGFLz3whG5eF6+kg0h6E0/4DDui9SAOvZZ1+JBjGM5ufu37mPheqDO3N3PpudvQbm8mChwdIvOT+qzsJ6ALBmg28uDutONPr82789j16gF3//+kx5F/44gb4rg3T4abdh810P1uzsx6JpXCIRsNqV5fhmefmj+MpKZbnSfgcr6ObyquHosCZhBbDuLW+WP/qwArAen4AVcHP1YP3jW+kibf2TnnXWJNJXVlbKbb4pw+YkrECb6xhW1GEXN1dnjTWhTnw6OgkryObqwZKmDWR4nYYVXHMFEVZg+1zBhBVQc10ES/F+8LsDGPsxTxHF/RdSP4KmDCuY5joNC4qtAChlVYEf+oJ6Aj4L9brLEGXUYlWXH8P8MKLUkqltt6IT4DofVhDNdQoWjtal71fXavBYq33/9csnO4cvt3cOM82m9MOrqgGPV3q1ahla1e1ahqVp1Wq3qxk/SPXo2LQugBVAc52GVa//sPfjbq229yPaW/v6MPXyxfaT7Z0X9RZiQMh8ZRoW0oxu1dAMt2sggxlGVasyVK9PH1bgzHWmGkpSDdV2nd0fa0pN2draTr3cQT/ZUMuoa1dN27QsZLguMk3LtPSqie2uhS0qSVOvhgHsc50NeKzwdFd2+RO2bWzzbOJ5DzHFoxwiivEeLPUeiJ+HLVb8N3NmI3IbBCtY5hrcdVAAiuJA5DcAHA9xr89NX/xkQ7xDxEchqbDCG4X9fXhSFKmpSzg6RViBSq7BsAoNx2k01p1GvuDgVrOF69FoM0oP7e2tncOdnZ16M4oP9p/vHzzfkA6eg1pSs7XQGs1al8AKkLkGwnLyhXweQOV/zqO80nrd/KXVev1Lix1mt3f+9WTr5XZdau5LBxsHB2hjYYM/A6nWwuvpwgqOuQbDWnfyeaWwDryUgtKqR5uvo6+jdXZ4CD2KnSfbP9WbzebBBhhrf+NA2jjY329Fp++s4JjrksziPVSFC+JLejtPQHuSILMO9r1kU7CycKDoTUmfcmZ5Eo3J1whjQ+WsYXjjpyh9bNG0W8PrBQsaQ0fhD+XNr2cnAaehmwNLKRSc9QZPrV///WbzSmjdJFjACdrEgrP75k15ksmFWwALKQ3kVUTo06O2OFgRMfrk0cyfPuFb2P/LEAGPjwcxx9Nb2BvjePNYvRPTgPXp3bt3P38Yu3v3zw/57mz/LmxEw5qH3WFgAZI25v2ENuWjnWaz6squLlcZo7TZnAKtmwWLLm9WNjc3H68ggBN93rWNqmkYlla1mtEp0LpZsJSKBLjiywtSvY4w47CMtKF1NYOF1fC0cHmlwm+HljFfIoItZrmmVtU002D6iEObmw/LC3UY8hxzgeEO5iHGqmxiUOPCIoRMAQUpFi/5mDFgOXwikD8cpf/wlLqoY8Aqlkql2OSsktls5xxafYfGyKw89LWcBvS4Ck57OoAmg0XULFtanByWTenWaVgkUuwDOA4sJ78O/fhC4TdnZfr90nFgpVBiCrCSS0vJU7BILpvKTgarcKx8flMZcOH7h0V64XXymZC+UOvbJ73Txz9+Zvkv/YtITkbyu/eOkVnexBbPKwUFy1lqp1RKqlC0oqqq/Iz3TBbVZKmUW/RLryaPYN/bLfLLiwQuKhL1SCWq9y54uUjUUqkTI3B1iSEG5yeBxUW5qa5gdDg2rFgiIcuMZTMkkltKLeWgrHIinSHwAo7L2RynlUnLspw9gt1FMw1Xp0ukk80mS+l0h1hpuJqU4GUmzZgsFwlJ8xZefpv7I8LCx1OlmMZXFqZPahJYRM/aGZuhBBRNRqwE2ZxGCZUUl2TTgv5ytgixlELMtZdykUiMUsTSiSUVaho2GQVYGUoBVpIhCpAZwnKMZDmsxNJ4sHCj4BQa+UahUV6JX+F81ljVUI1B0pQY1vmWR01HxgiKmeMJhBBLEqJjuUPIYixCMgylkjFSjPFYwrJVUvtgmcVYCcIqSYrwii0We/2S0WApeYXf3lkHWJvx6Yf7ZLBIBMLJpSgN3BIoUYTSM968kcVcsqQjesRhYVvlgR3L8te9DOccIz1YMnwYvLIxdYkf8G//pFEzqwCuKkAPi1bKm1c3nzVWNczJKTkhI5SFQ2ChJEmjNI/mo6ycSlDESXSyCKcY1Cp4S0p9C8trHt45i3HCPNonhXV8c4ffvvfns3qTWP6ziLGhDwuCHDMzCWXksJIydYsJanLDQHkzHZeTiJBOAqpm4oi/JeW3cR6s2Lmw5ElhcV7eKgdvERuWJNP1x4a2zZc8NEXASnJAZMsrK1AAWJHFLNQtJkMLWMwiWeUpxmFFSCxnQ9YvFhOIJckFsDgeYlFAzU+mJoFFl1c2V8rle2V+q6vZ7FLTNjXXNKqaabuvR7xLODEsCPUiwtQmZAnx8O54zooQyC4dpWN+fC0SHkEAC3bIopdn8G8N9ZBH1Tmw4HPUNG9QvZPFCTqltCItxyuV5QWp1YK/pqVpllY1DLcKG21h4tWoo8EiRwmawLy7QPj6nU4pgXxYUErEuJfAWdTN8XWKQMK0k7kMRdkY6aShT2C7LHcOLJRyMyn4HDgKvQ8ks9x4/SyQsrkSL1cqlTKKchtpmmZUrapmaFq1a+P6e4aVgcpPEy6URl2iWE7DaHiJFz2WpTTNI5xkEpimEjZjcNES8/qtkPDQ+WSQHqkSySVo1ocly8ewGKOYZb132zLGifFheZlFqYL9TKeUT7/DhlGkMTzBMrbxYOkUmV5hiLpF9VIkk3F5cpOSlbH8MiZ1aueKbiZDYh1XR3pG9S/P2EiHt6puxvVglTJW0odVhEqc8Wtf7AjeoY4f8BeLTqE5HDGzCOkfPfMM6nsVOX2ckFPjaP9E3zv81vDsaHvcaphHfu8BeqiBGBtOVV4P/qIJ0/GmaNYLeRj0rDeuYMQTAFhsmrCcvLdIq/Bb4Qru4AuHdWK6b1JYyOG38Buo4TjOzXNWZFEtqhedGxmW4q/BUvwly1NnJRzWII08NmwoSqPBZ7Ua/ld6QlgXwnIgrSDYC06jkc9zaCGsAfNZP+fX83mH36zIN5zpo7pJsPjy7kIBXMV95RRCZw0xn4V72+mzui6whlrMhvxvpRzPZ4Gw7a0POX7o+O0SZe9bPFcEKxAabslRu63rKO4thK/XjaphId1mJlCDh2bYQE+3ERxHts7qI37B4iatKQXRePnecryy3Pbms2wty2ezDMMyuma3m4Bnw2Balxlpw3ylVS084gK3mwVLqZSXy/F4vK03m4Cuaxk28DmeAWSaDvtVTZM5tS57Jd9uWAi3db56pu3fq6AmP2Yz78HjS9epzZ9shnS3Sm8prLeLkfGJ5MbvDuL+Q17Ujz7DdTNgYWkai5FvCSyEp7BiNIQVwjorafzfP3D7YF3FaqwbBQtaQN37Aut3/CZF6Kx+nfklGFGpvru6W9utrdZW11bfR3N4bWHhVnOhvldbU9ZW9/bW1pA06kDvNsFCqF5vrq7t7YG1vl/bq03w+3huAyzeK/fuTzg1nllhNezTgNbwahZF3lBY70shrBBWCCuENbRCWCPoLaz3Mgy8CNadawLrwZwHC33wQJS8/5bhesD6pgdL4H/4cf9pYGHNn9SzD4Tr2cNTCgys+f98eUJf3Btdy/EBGv3jvjitM7SEwfrvhxPr8SBN/vEf3ihYV63gwPrio8ArMLBmZj5Vf/99jKB6T3r8PzVArSE0hzMPZz//QzD1+XmNoeB+1lcDOjUBlFhYMx+LLv9IEgzreplLNKtrZS7RqLiujblEg/L0SDSFISWa07Guh7lEU+rpWphLNKR3ugbmEo2oT48um3sTLtGETijo5hLN56QCbi7ReE4r0OYSDeeMgmwu0WzOUXDNJZrMeQqsuUSDOV8BHVyLxnKBgmku0VQuVBDNJZrJxQqguUQjGaTAmUs0kIEKmrlE87hEwTKXaBqXKkjmEs3icgXIXKJRDKPAmEs0iKEUFHOJ5jCkgmEu0RSGVSDMJRrC8ArAzI1oBCNIvLlEExhJos0luvyjSbC5RBd/VAk1l+jCjyyR5hJd9jEkzlz/B7bXbf8ylfyyAAAAAElFTkSuQmCC");
        articles.add(article1);
        articles.add(article2);
        articles.add(article3);
    }

    public List<Article> getAllArticle(){
        return articles;
    }

    public void addNewArticle(Article article){
        try {
            article.setId((articles.get(articles.size()-1).getId())+1);
            articles.add(article);
        }catch (IndexOutOfBoundsException e){
            article.setId(articles.size() +1);
            articles.add(article);
        }
    }

    public Article findArticleById(int id){
        return articles.stream().filter(article -> article.getId()==id).findFirst().orElseThrow();
    }

    public void deleteArticleById(int id) {
        for (int i = 0; i < articles.size(); i++) {
            if (articles.get(i).getId() == id) {
                articles.remove(i);
                break;
            }
        }
    }

    public void updateArticleById(int id, Article article) {
        for (int i = 0; i < articles.size(); i++) {
            if (articles.get(i).getId() == id) {
                articles.set(i, article);
                break;
            }
        }
        System.out.println(articles.toString());
    }
}
