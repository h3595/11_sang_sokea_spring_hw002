package com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.controller;

import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.model.Article;
import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.ArticleService;
import com.springboothw2._11_sr_sang_sokea_spring_boot_hw2.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;

@Controller
public class ArticleController {
    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    ArticleService articleService;

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles", articleService.getAllArticle());
        return "index";
    }

    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("article", new Article());
        return "form-add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Article article, BindingResult bindingResult){
        if(article.getFile().isEmpty()){
            article.setUrlImg("http://localhost:8080/images/defaulImage.png");
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
                System.out.println("filename: " + filename);
                article.setUrlImg(filename);
            }catch (Exception e){
                System.out.println("Error with the images upload" + e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "/form-add";
        }
        articleService.addArticleMethod(article);

        return "redirect:/";
    }

    @GetMapping("/view-article/{id}")
    public String viewArticle(@PathVariable int id,Model model){
        Article resultArticle = articleService.findArticleById(id);

        System.out.println("result article : " + resultArticle);
        model.addAttribute("article", resultArticle);

        return "form-view";
    }

    @GetMapping("/update-article/{id}")
    public String updateArticle(@PathVariable int id, Model model){
        Article resultArticle = articleService.findArticleById(id);
        System.out.println("result article : " + resultArticle);
        model.addAttribute("article", resultArticle);
        model.addAttribute("id", id);
        return "form-update";
    }

    @GetMapping("/delete-article/{id}")
    public String deleteArticle(@PathVariable int id, Model model){
        articleService.deleteArticleMethod(id);
        return "redirect:/";
    }

    @PostMapping("/handle-update/{id}")
    public String handleUpdate(@PathVariable int id, @ModelAttribute @Valid Article article, BindingResult bindingResult){
        Article resultArticle = articleService.findArticleById(id);
        if(article.getFile().isEmpty()){
            article.setUrlImg(resultArticle.getUrlImg());
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
                System.out.println("filename: " + filename);
                System.out.println(article);
                article.setUrlImg(filename);
//                delete file after update
//                File folder = new File("src/main/resources/images");
//                File[] listOfFiles = folder.listFiles();
//
//                for (int i = 0; i < listOfFiles.length; i++) {
//                    if (listOfFiles[i].isFile()) {
//                        if(listOfFiles[i].getName().contains(fileStorageService.saveFile(article.getFile()))){
//                            File file = new File(fileStorageService.saveFile(article.getFile()));
//                            if(file.delete())   // delete() will delete the selected file from system and return true if deletes successfully else it'll return false
//                            {
//                                System.out.println("Selected File deleted successfully");
//                            }
//                            else
//                            {
//                                System.out.println("Failed to delete the selected file");
//                            }
//                        }
//                    } else if (listOfFiles[i].isDirectory()) {
//                        System.out.println("Directory " + listOfFiles[i].getName());
//                    }
//                }
            }catch (Exception e){
                System.out.println("Error with the images upload" + e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "/form-update";
        }
        articleService.updateArticleMethod(id, article);

        return "redirect:/";
    }
}
